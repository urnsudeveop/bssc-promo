<?php
require_once( __DIR__.'/../instance/TPromos.php');
//$uWPQuery = TPromos::getInstance();

class MainPromo extends TPromos{
	public OBJECT $wpAjax;
	protected u_tABLE $WP_List_Table;
	private $current_user_posts="";
	private $cols=[];
	private $total=0;
	private $promoData;
	public function __construct(){
  
		
	parent::__construct();
add_action("WP_HTML_TABL3",[$this,  'loaderPostTable']);	
	}
	public function loaderPostTable()
	{
								 global $c_ols;
								 $c_ols=$this->GetCols();       /*
$this->SetWPListTable(new u_tABLE([
'singular' => 'Promoción', //singular name of the listed records
'plural' => 'Promociones', //plural name of the listed records
'ajax' => false //should this table support ajax?

]));                   */
	
	
	
	$this->getPromociones();
	//$this->WPListTable()->setColumns($this->GetCols());
//	$this->WPListTable()->Display();
	}	

	
public function sample_admin_notice__error() {
    $class = 'notice notice-error';
    $message = __( 'No tiene Sedes asignadas a su cargo, contacte con el administrador' );
 
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}
	public function pre(){
		
	
		
add_action( 'admin_noticesA',[$this, 'sample_admin_notice__error' ]); 
		
		
		
		
		
		
		global $uPoztType,$capMaster;
		
$roles=	u_get_current_user_roles();
$lista_sedes=[];
$autorizado=false;

global $cps,$user_creden;
				
				$user_creden["cps"]=$cps;
				
	if (in_array($cps, $roles)){
		
		
		
	$sedes=get_user_meta(get_current_user_id() ,"SEDE")[0];
		if(  !( isset($sedes) AND is_array($sedes) )  ) : 
		$sedes=[]; 
		$_sed=["sin sede asignada"];endif;

	foreach($sedes as $i=>$sede_post_id):
	$id=$sede_post_id;
	$content_post = get_post($id);
	$_sed[]=$content_post->post_title;
	
	$lista_sedes[ $content_post->post_title
	][ $content_post->post_name
	]=$id;

	$r=wp_insert_term($content_post->post_title, 'Sedes', $content_post->post_name);

	endforeach;
	$autorizado=true;
	}else{
		
	}
	$toast_msg=false;
	
	if($autorizado){
		
		if( count($sedes)==0):
		do_action( 'admin_noticesA');
		$toast_msg = "No tiene Sedes asignadas a su cargo, contacte con el administrador";
			endif;

$user_creden["sedes"]=$_sed;
$rows=$this->get_posts_();
$tavObj =	do_action("WP_HTML_TABL3");
//var_dump($rows);
							  $this->TPromoData($rows);
$tdata=(json_encode($this->getPromoData()));
	pLoader()->view("MainPromo.php",["grados"=>strToLi(),"sedes"=>$lista_sedes,"_sed"=>base64_encode(json_encode($_sed)),"sedes_ids"=>$sedes,"tdata"=>$tdata,"toast_msg"=>$toast_msg]);
	}
	
	
	}
	

    /**
     * @param  $wpAjax
     *
     * @return \MainPromo
     */
	 
	 
	 public function get_posts_(){
										       global $current_user;
		 $args = array(
  'author'        =>  $current_user->ID, 
  'orderby'       =>  'post_date',
  'order'         =>  'DESC',
  'post_type'        => 'type-promociones',
   'post_status'=>['future','publish','pending'],
  'posts_per_page' => -1 // no limit
);

									   $this->SetCurrentUserPosts(get_posts( $args ));
$this->SetTotal( count($this->GetCurrentUserPosts()));
$data=         $this->GetCurrentUserPosts()[0];
$this->SetCols( $data->post_content);
return $this->GetCurrentUserPosts();
	 }
	 
	 
	 
    public function SetWpAjax(Object $wpAjax)
    {
        $this->wpAjax = $wpAjax;

        return $this;
    }

    /**
     * @return 
     */
    public function WpAjax()
    {
        return $this->wpAjax;
    }

    /**
     * @param  $WP_List_Table
     *
     * @return \MainPromo
     */
    public function SetWPListTable(u_tABLE $WP_List_Table)
    {
        $this->WP_List_Table = $WP_List_Table;

        return $this;
    }

    /**
     * @return 
     */
    public function WPListTable()
    {
        //return $this->WP_List_Table;
    }

    /**
     * @param  $current_user_posts
     *
     * @return \MainPromo
     */
    public function SetCurrentUserPosts($current_user_posts)
    {
        $this->current_user_posts = $current_user_posts;

        return $this;
    }

    /**
     * @return 
     */
    public function GetCurrentUserPosts()
    {
        return $this->current_user_posts;
    }

    /**
     * @param  $cols
     *
     * @return \MainPromo
     */
    public function SetCols($cols,$json=true)
    {
		$tcols=[];
		if($json):
			$data=	 json_decode($cols);
			
			foreach($data as $k=>$v){
				  $tcols[$v->name]=$v->name;
			}                                
			
			
		endif;
													  
													// print_r($tcols);
        $this->cols = $tcols;

        return $this;
    }

    /**
     * @return 
     */
    public function GetCols()
    {
        return $this->cols;
    }

    /**
     * @param  $total
     *
     * @return \MainPromo
     */
    public function SetTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return 
     */
    public function GetTotal()
    {
        return $this->total;
    }

    /**
     * @param  $promoData
     *
     * @return \MainPromo
     */
    public function TPromoData($promoData)
    {
			$st=[];
		foreach($promoData as $k => $rows )         {
							foreach($rows as $d=>$c){
											$data_="";
											if($d=="post_content"){
												$data_=
												(array) json_decode($c,false);
												
												foreach($data_ as $d_=>$c_){
													$rows->{$c_->name}=utf8_decode($c_->value);
												}
												
												$st[]=(array) $rows;
											}
							}
			
					}
		
        $this->promoData = $st;

        return $this;
    }

    /**
     * @return 
     */
    public function getPromoData()
    {
        return $this->promoData;
    }
}