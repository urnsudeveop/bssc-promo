<?php

class s1nglet0n {
	
	/** @var self */
	protected static $instance;

	protected function __construct() {
	}
	
	/**
	 * @return self
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
