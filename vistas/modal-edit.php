			   <style>
					 .modal-dialog {
    min-width: 70vw;
    margin: 4% 22%;
        margin-top: 4%;
        margin-right: 22%;
        margin-bottom: 4%;
        margin-left: 22%;
}

.modal-header {
    background: #3858e9;
}
.btn-close {
    background-color: #eaecf1b3;
}
			   </style>
<!--Modal -->
    <div class="modal" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">
<!-- M.Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Editar</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
<!-- M.body -->
<div class="modal-body">
  
 <?php require_once(__DIR__."/formEditar.php");  ?> 
  
  
</div>
<!-- M.footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-primary"  data-bs-dismiss="modal">Add</button>
            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
