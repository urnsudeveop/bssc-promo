
<link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"
    />
<!--tabla -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link href="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.css" rel="stylesheet">
<script src="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.js"></script>

<div class="container tabladata">
<table id="table"
data-height="460"
  data-pagination="true"
  data-sortable="true"
  data-search="true"
   a-data-card-view="true"
    data-toolbar="#toolbar"
  data-show-search-button="true"
  style="font-size: 0.65rem;"
  data-id-field="ID"
   data-unique-id="ID"
  data-select-item-name="selectItemName"
>
  <thead style="font-size: 0.65rem;font-weight:bold;background: #025;color: white;">
    <tr>
      
      <th data-width="100" data-sortable="true" data-field="nombres">Nombres</th>
      <th   data-width="100" data-sortable="true" data-field="paterno">Apellido Paterno</th>
      <th  data-width="100" data-sortable="true" data-field="materno">Apellido Materno</th>
      <th data-sortable="true" data-field="grado">Grado</th>
      <th data-sortable="true" data-field="inicio">Inicio</th>
      <th data-field="fin">Término</th>
	<th   data-id-field="ID" data-field="ID" data-formatter="opciones">Opciones</th>
     
    </tr>
  </thead>
</table>
</div>

<script>



  var $table = jQuery('#table')
	    var data = null;
  jQuery(function($) {
     data =<?php   echo $tdata; ?>
	 
    $table.bootstrapTable({data: data});

  })
  
 // urnus-scr
 <?php require_once(__DIR__."/js/load-row.js");  ?>
 </script>
<!-- fintabla-->

<?php require_once(__DIR__."/modal-edit.php");  ?>
 