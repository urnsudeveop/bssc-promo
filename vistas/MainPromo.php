<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat');

* {
    box-sizing: border-box;
}

.customcss {
    background-color: #CDCAD730;
    font-family: Montserrat, sans-serif;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding-top: 5px;
    margin: 0;
}

h3 {
    margin: 10px 0;
}

h6 {
    margin: 5px 0;
    text-transform: uppercase;
}

p {
    font-size: 14px;
    line-height: 21px;
}

.card-container {
	background-color: #025;
	border-radius: 5px;
	box-shadow: 0px 10px 20px -10px rgb(0, 0, 0);
	color: #FCFDFE;
	padding-top: 30px;
	position: relative;
	width: 100%;
	max-width: 100%;
	text-align: center;
}

.card-container .pro {
    color: #231E39;
    background-color: #FEBB0B;
    border-radius: 3px;
    font-size: 14px;
    font-weight: bold;
    padding: 3px 7px;
    position: absolute;
    top: 30px;
    left: 30px;
}

.card-container .round {
    border: 1px solid #03BFCB;
    border-radius: 50%;
    padding: 7px;
}

button.primary {
    background-color: #03BFCB;
    border: 1px solid #03BFCB;
    border-radius: 3px;
    color: #231E39;
    font-family: Montserrat, sans-serif;
    font-weight: 500;
    padding: 10px 25px;
}

button.primary.ghost {
    background-color: transparent;
    color: #02899C;
}

.skills {
    background-color: #1D2327;
    text-align: left;
    padding: 15px;
    margin-top: 30px;
}

.skills ulc {
    list-style-type: none;
    margin: 0;
    padding: 0;
}

.skills ulc lic {
    border: 1px solid #2D2747;
    border-radius: 2px;
    display: inline-block;
    font-size: 12px;
    margin: 0 7px 7px 0;
    padding: 7px;
}

.card-container > h3 {
	color: white;
	font-weight: bold;
}

.container.tabladata {

background: white !important;
}

.buttons{
	padding-top: 2em;
}
</style>
<div class="container customcss">
<div class="card-container">

		<div class="container-fluid mb-2">
<?php 
global $user_creden;
foreach($user_creden["sedes"] as $sede){
	?>
	<span class="badge bg-primary"><?php echo $sede;?></span>
	<?php
	
}
?>
		  </div>
<?php

if(!is_bool($toast_msg)){
	
 require_once(__DIR__."/html/toast.html");
 
}
require_once(__DIR__."/tablaPromos.php")
?>
   
   <div class="buttons">
     
	 
    </div>
    <div class="skills">
       
		<?php 
//require_once(__DIR__."/formIngreso.php")
?>
   
    </div>
</div>

	<?php 
require_once(__DIR__."/modal-agregar.php")
?>
</div>