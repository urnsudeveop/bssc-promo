<div id="toolbar">
<div class="form-inline" role="form">
   <button class="btn btn-secondary custbtn" type="button" id="nuevo"  data-bs-toggle="modal" data-bs-target="#agregarModal">
   Agregar
  </button>

</div>
</div>
<span class="badge bg-primary">Primary</span>									
<form id="form-promo" action="#" method="post" onsubmit="return validation();">
 <div >
 		<div class="row">
		<div class="col-md-4 col-xd-12">
		
		<div class="input-group mb-3">
  <span class="input-group-text" id="nombres">Nombres</span>
  <input type="text" class="form-control" name="nombres" placeholder="Nombres" aria-label="nombres" aria-describedby="nombres">
</div>

		
		</div>
		<div class="col-md-4 col-sx-12">
		<div class="input-group mb-3">
  <span class="input-group-text" id="paterno">Apellido Paterno</span>
  <input type="text" class="form-control" name="paterno" placeholder="Paterno" aria-label="paterno" aria-describedby="paterno">
</div>
		</div>
		<div class="col-4">
		<div class="input-group mb-3">
  <span class="input-group-text" id="materno">Apellido Materno</span>
  <input type="text" class="form-control" name="materno" placeholder="Materno" aria-label="materno" aria-describedby="materno">
</div>
		</div>
		</div>
		<div class="row">
		
		<div class="col-4">
		<div class="input-group mb-3">
  <span class="input-group-text" id="inicio">Inicio</span>
  <input type="date" class="form-control" name="inicio" placeholder="inicio" aria-label="inicio" aria-describedby="inicio">
</div>
		</div>
		<div class="col-4">
		<div class="input-group mb-3">
  <span class="input-group-text" id="fin">Término</span>
  <input type="date" class="form-control" name="fin" placeholder="fin" aria-label="fin" aria-describedby="fin">
</div>
		</div>
		<div class="col-4">
		<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle custbtn" type="button" id="seleccGrado" data-bs-toggle="dropdown" aria-expanded="false">
   Grado
  </button>
  <ul class="dropdown-menu custul" aria-labelledby="seleccGrado">
   
<?php echo $grados;?> 
 </ul>
</div>
</div>
		</div>
		
		
		
 </div>
 
<div class="container-fluid">
   
   <button class="btn btn-secondary custbtn" type="submit" id="guardar" >
   Guardar
  </button>
 
</div>
<input type="hidden"  id="grado"  name="grado"/>
<input type="hidden"  id="sedes_ids"  name="sedes_ids" value="<?php echo base64_encode(json_encode($sedes_ids));?>"/>
<input type="hidden"  id="sedes"  name="sedes" value="<?php echo base64_encode(serialize($sedes));?>"/>
<input type="hidden"  id="sed"  name="sed" value="<?php echo $_sed;?>"/>
 </form>
 
 <script>
 jQuery(document).ready(
 function($){
	 
	 $(".opt-li-cust-dropdown").on("click",function(){
	$("#grado").val($(this).attr("at-cust"));	
$("#seleccGrado").text($(this).attr("at-cust"));		 
		 }) 
		 
$(".opcion").on("click",function(e){
		e.preventDefault();
		
})
	

$("#form-promo").submit(function(e) {

    e.preventDefault();  
var formData = JSON.stringify( $('#form-promo').serializeArray());

 var fd = new FormData();
  
  fd.append("action", "SavePromocionPost_Endpoint_private");
  fd.append("form", formData);

console.log(ajaxurl);

        jQuery.ajax({
            type: "POST",
            url: ajaxurl+"?XDEBUG_SESSION=codelite",
            processData: false,
            contentType: false,
            data: fd,
            beforeSend: function () {
            },
		success: function (response) {
			console.log(response)}
		})


		
 

 });
 //$("#form-promo").submit();
 }

 );
 
 </script>
 